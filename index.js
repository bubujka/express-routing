var express = require('express');
var bodyParser = require('body-parser');
var db = require('./db');

var app = express();

app.set('views', __dirname+'/tpl');
app.set('view engine', 'jade');

app.locals.pretty = true;
app.locals.copyright = 'sdelala lena v 2016 gody';

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

app.post('/', function(req,res){
  res.json(req.body);
});
app.get('/', function(req,res){
  res.render('index', {
    name: 'lena'
  });
});

app.get('/news', function(req,res,next){
  db.find_all_news(function(err, news){
    if(err){ return next(err); }

    res.render('news/all', { data: news });
  });
});
app.get('/news/:id', function(req, res, next){
  db.find_news_by_id(req.params.id, function(err,data){
    if(err){
      return next(err);
    }
    if(data.template){
      res.render('news/'+data.template, data);
    }else{
      res.render('news/one', data);
    }
  });
});

app.get('/hello/:id/:name', function(req,res){
  res.json({
    params: req.params,
    query: req.query,
  });
});

app.listen(3000);
