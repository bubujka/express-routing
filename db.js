var news = require('./news.json');
var request = require('request');

module.exports = {
  find_news_by_id: function(id, next){
    request.get('http://metropolis.moscow/api/news/id/'+id, function(err,res,body){
      next(null, JSON.parse(body));
    });
//   for(var i in news.data){
//     if(news.data[i].id === id){
//       return next(null,  news.data[i]);
//     }
//   }
//   next(new Error('Id '+id+' not found'));
  },
  find_all_news: function(next){
    request.post('http://metropolis.moscow/api/news/short?limit=100', function(err,res,body){
      if(err){ return next(err); }
      next(null, JSON.parse(body).data);
    });
  }

};


